import Helmet from 'react-helmet';
import React from 'react';
import {RouteTransition} from 'react-router-transition';
import Nav from './app/nav';
import {name} from '../package.json';

export default props => (
   <div className='app'>
      <Helmet titleTemplate={`${name} · %s`} />
      <Nav path={props.location.pathname} />
      <RouteTransition
         pathname={props.location.pathname}
         atEnter={{opacity: 0}}
         atLeave={{opacity: 0}}
         atActive={{opacity: 1}}
         className='views'
      >{props.children}</RouteTransition>
   </div>
);
