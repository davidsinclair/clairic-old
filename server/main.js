import Hapi from 'hapi';
import frontend from './frontend';
import gutil from 'gulp-util';
import {host, serverPort as port, isWatching} from '../universal/config';
import Inert from 'inert';

const server = new Hapi.Server();

server.connection({host, port});
server.route(frontend.getHtml.default);

if(isWatching) {
   server.register(Inert, () => {});
   server.route(frontend.getFile.default);
   server.ext('onPreResponse', (request, reply) => {
      if(request.response.isBoom) {
         return frontend.getHtml.default.handler(request, reply);
      }

      return reply.continue();
   });
}

server.start(() => {gutil.log(`API & Frontend: ${server.info.uri}`);});
