export const isWatching = process.env.WATCH === 'true';
export const isProduction = process.env.NODE_ENV === 'production';
export const host = '127.0.0.1';
export const serverPort = 5000;
export const browserHotPort = 8888;
export const root = process.cwd();
