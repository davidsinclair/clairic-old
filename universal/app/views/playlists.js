import Helmet from 'react-helmet';
//import Infinite from 'react-infinite';
import React from 'react';

export default () => (
   <div>
      <Helmet title='playlists' />
      Playlists
   </div>
);

//<Infinite containerHeight={100} elementHeight={40}>
//	{props? props.list.map((o, i) => (<li key={i}>{o}</li>)) : null}
//</Infinite>
