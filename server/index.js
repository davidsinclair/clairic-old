require('babel-register');

const config = require('../universal/config');
const WebpackIsomorphicTools = require('webpack-isomorphic-tools');
const wITConfig = require('./wITConfig').default;

global.webpackIsomorphicTools = new WebpackIsomorphicTools(wITConfig)
   .development(config.isWatching)
   .server(config.root, () => {require('./main');});
