import {Link} from 'react-router';
import React from 'react';

const data = {
   'iconType': 'self',
   'items': [
      {'name': 'player'},
      {'name': 'songs'},
      {'name': 'artists'},
      {'name': 'albums'},
      {'name': 'videos'},
      {
         'name': 'playlists',
         'iconType': 'parent',
         'items': [
            {'name': 'Playlists The first'},
            {'name': 'Second playlist'},
            {'name': 'Third playlist'},
            {'name': 'Playlist'},
            {'name': 'Playlists The first'},
            {'name': 'Second playlist'},
            {'name': 'Third playlist'}
         ]
      },
      {'name': 'settings'}
   ]
};

const ListItem = props => {
   let liInner = [];
   let linkClass;

   if(props.iconType === 'self') {
      linkClass = 'icon-' + props.item.name;
   } else if(props.iconType === 'parent') {
      linkClass = 'icon-' + props.parentName;
   }

   if(props.parentName !== 'playlists') {
      liInner.push(
         <Link
            key='link'
            to={props.item.name === 'player' ?
               '/' : '/' + props.item.name
            }
            activeClassName={
               props.item.name === 'player' && props.path !== '/' ?
                  '' : 'active'
            }
            className={linkClass}
         >
            <span>{props.item.name}</span>
         </Link>
      );
   } else {
      liInner.push(
         <a key='link' href='#' className={linkClass}>
            <span>{props.item.name}</span>
         </a>
      );
   }

   if(props.item.items) {
      liInner.push(
         <List
            key={props.item.name}
            name={props.item.name}
            iconType={props.item.iconType}
            items={props.item.items}
         />
      );
   }

   return <li>{liInner}</li>;
};

const List = props => (
   <ul>
      {props.items.map((o, i) => {
         return (
            <ListItem
               key={i}
               item={o}
               iconType={props.iconType}
               parentName={props.name}
               path={props.path}
            />
         );
      })}
   </ul>
);

export default ({path}) => (
   <nav className='nav'><List {...data} path={path} /></nav>
);
