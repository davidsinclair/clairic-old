/* eslint-disable max-len */

import React from 'react';

export default ({mainHtml, helmet, styleHtml}) => (
   <html>
      <head>
         {helmet.title.toComponent()}
         <meta name='application-name' content='clairic' />
         <meta name='description' content='a cloud music player' />
         <meta charSet='utf-8' />
         <meta name='author' content='David Sinclair' />
         <meta name='apple-mobile-web-app-capable' content='yes' />
         <meta name='apple-mobile-web-app-status-bar-style' content='default' />
         <meta name='format-detection' content='telephone=no' />
         <meta name='robots' content='index, follow' />
         <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0' />
         {styleHtml}
         <link rel='apple-touch-icon' sizes='180x180' href='/images/favicons/apple-touch-icon.png' />
         <link rel='icon' type='image/png' href='/images/favicons/favicon-32x32.png' sizes='32x32' />
         <link rel='icon' type='image/png' href='/images/favicons/favicon-16x16.png' sizes='16x16' />
         <link rel='manifest' href='/images/favicons/manifest.json' />
         <link rel='mask-icon' href='/images/favicons/safari-pinned-tab.svg' color='#0c5400' />
         <link rel='shortcut icon' href='/images/favicons/favicon.ico' />
         <meta name='msapplication-config' content='/images/favicons/browserconfig.xml' />
         <meta name='theme-color' content='#ffffff' />
      </head>
      <body>
         <div id='main' dangerouslySetInnerHTML={{__html: mainHtml}} />
      </body>
   </html>
);
/* eslint-enable max-len */
