import webpack from 'webpack';
import webpackConfig from '../../../webpack.config.js';
import gutil from 'gulp-util';
import Hapi from 'hapi';
import WebpackPlugin from 'hapi-webpack-plugin';
import {host, browserHotPort as port} from '../../../universal/config';

const compiler = webpack(webpackConfig);
const server = new Hapi.Server();

server.connection({host, port});
server.register(
   {
      register: WebpackPlugin,
      options: {
         compiler,
         hot: {log: false},
         assets: {
            headers: {'Access-Control-Allow-Origin': '*'},
            quiet: true,
            publicPath: webpackConfig.output.publicPath
         }
      }
   },
   error => {
      if(error) {
         return gutil.log(error);
      }
      server.start(() => gutil.log(`Hot: ${server.info.uri}`));
   }
);
