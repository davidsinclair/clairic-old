# clairic
a cloud music player for the web

## npm
```
npm i
npm start
npm start -- -d
npm start -- -w
npm start -- -wd
npm run server:start
npm run server:stop
npm run server:show
npm run server:list
npm run updater
```

## docker
```
eval "$(docker-machine env default)"
chmod +x docker/start.sh; ./docker/start.sh;
chmod +x docker/stop.sh; ./docker/stop.sh;
```

## notes
* in production mode, nginx handles file requests and passes all others to hapi
* in development mode, hapi handles file, html, and api requests
