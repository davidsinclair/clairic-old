import React from 'react';
import {IndexRoute, Route} from 'react-router';
import App from './app';
import Albums from './app/views/albums';
import Artists from './app/views/artists';
import Fallback from './app/views/fallback';
import Songs from './app/views/songs';
import Player from './app/views/player';
import Playlists from './app/views/playlists';
import Settings from './app/views/settings';
import Videos from './app/views/videos';

export default (
   <Route path='/' component={App}>
      <IndexRoute component={Player} />
      <Route path='songs' component={Songs} />
      <Route path='artists' component={Artists} />
      <Route path='albums' component={Albums} />
      <Route path='videos' component={Videos} />
      <Route path='playlists' component={Playlists} />
      <Route path='settings' component={Settings} />
      <Route path='*' component={Fallback} />
   </Route>
);
