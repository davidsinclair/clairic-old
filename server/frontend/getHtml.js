import {match, RouterContext} from 'react-router';
import routes from '../../universal/routes';
import {renderToStaticMarkup, renderToString} from 'react-dom/server';
import Helmet from 'react-helmet';
import React from 'react';
import Html from './getHtml/html';
import {isWatching} from '../../universal/config';
import {version} from '../../package.json';

export default {
   method: 'get',
   path: isWatching? '/' : '/{p*}',
   handler: (request, reply) => {
      match(
         {routes, location: request.url.path},
         (error, redirectLocation, renderProps) => {
            const appHtml = renderToString(<RouterContext {...renderProps} />);
            const {
               styles: {main: cssFile},
               javascript: {main: jsFile}
            } = webpackIsomorphicTools.assets();
            const scriptHtml =
               `<script async src="${jsFile}?v=${version}"></script>`;
            const styleHtml = cssFile? (
               <link rel='stylesheet' href={`${cssFile}?v=${version}`} />
            ) : null;
            const helmet = Helmet.rewind();

            if(isWatching) {
               webpackIsomorphicTools.refresh();
            }

            const response = '<!doctype html>' + renderToStaticMarkup(
               <Html
                  styleHtml={styleHtml}
                  helmet={helmet}
                  mainHtml={`<div id='app'>${appHtml}</div>${scriptHtml}`}
                  script
               />
            );

            if(redirectLocation) {
               return reply(response).code(301);
            } else if(error) {
               return reply(response).code(505);
            } else {
               const status = renderProps.routes
                  .some(route => route.path === '*') ? 404 : 200;
               return reply(response).code(status);
            }
         }
      );
   },
   config: {
      description: 'renders a React page',
      notes: 'used for all pages',
      tags: ['react']
   }
};
