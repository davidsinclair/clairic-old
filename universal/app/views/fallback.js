import React from 'react';
import Helmet from 'react-helmet';

export default () => (
   <div>
      <Helmet title='error' />
      <p>
         Oh, no! You may not have been aware,
         but this page is neither here nor there.
      </p>
   </div>
);
