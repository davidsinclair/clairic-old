import Helmet from 'react-helmet';
import React from 'react';

export default () => (
   <div>
      <Helmet title='artists' />
      <p>Artists</p>
   </div>
);
